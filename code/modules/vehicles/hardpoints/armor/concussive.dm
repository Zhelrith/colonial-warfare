/obj/item/hardpoint/buff/armor/concussive
	name = "Concussive Armor"
	desc = "Protects the vehicle from high-impact weapons"

	icon_state = "concussive_armor"
	disp_icon = "tank"
	disp_icon_state = "concussive_armor"

	point_cost = 600
	health = 1000

	type_multipliers = list(
		"blunt" = 0.67,
		"all" = 0.9
	)
